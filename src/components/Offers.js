import SingleOffer from "./subComponents/SingleOffer";
import "./CSS/offers.css";

const Offers = () => {
  const offers = [
    {
      name: "Making your website",
      price: "$1000",
      isNew: true,
      key: 0,
    },
    {
      name: "Cloud Services",
      price: "$3500",
      isNew: true,
      key: 1,
    },
    {
      name: "It team outsourcing",
      price: "$200000",
      isNew: true,
      key: 2,
    },
    {
      name: "Hosting your app",
      price: "$100",
      isNew: false,
      key: 3,
    },
    {
      name: "Buying a domain",
      price: "$250",
      isNew: false,
      key: 4,
    },
    {
      name: "A free smile",
      price: "$0",
      isNew: false,
      key: 5,
    },
  ];
  return (
    <section className="offers-section" id="offerSection">
      <div className="offers-section-container">
        <h1 className="offers-title">What our company does?</h1>
        <div className="offers-display">
          {offers.map(({ name, price, isNew, key }) => (
            <SingleOffer name={name} price={price} isNew={isNew} key={key} />
          ))}
        </div>
      </div>
    </section>
  );
};
export default Offers;
