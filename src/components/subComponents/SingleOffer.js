import "../CSS/singleOffer.css";

const SingleOffer = ({ isNew, name, price }) => {
  return (
    <div className={isNew ? "offer dot-parent" : "offer"}>
      {isNew && <div className="dot" />}
      <div className="offer-content-wrapper">
        <h2>{name}</h2>
        <p>{price}</p>
        <p>{isNew ? "Brand new" : "Standard but excellent"}</p>
      </div>
    </div>
  );
};

export default SingleOffer;
